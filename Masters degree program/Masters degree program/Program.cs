﻿using System.Collections.Generic;
using System.Linq;

namespace Masters_degree_program
{
    class Program
    {
        static void Main(string[] args)
        {
            var test_dispatcher = new TestDispatcher();

            //test_dispatcher.BeginTest("data.txt", TestManagersFactory.GetTestManagers().Last());
            //test_dispatcher.BeginTest("data.txt", new List<ITestManager>() { new NaiveBayesTestManager(false)});
           test_dispatcher.BeginTest("data.txt", new List<ITestManager>() { new NeuronNetworkTestManager(true) });
            //test_dispatcher.BeginTest("data.txt", new NearestMeanTestManager());
        }
    }
}
