﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms.Extenders;
using Bytescout.Spreadsheet;
using DataSaving.Common;
using FileDataReader;

namespace Masters_degree_program
{
    public sealed class TestDispatcher
    {
        public void BeginTest(string data_file_name, IEnumerable<ITestManager> test_managers)
        {
            var test = new FileDataReader.FileDataReader();
            var data = test.GetDataFromFile(data_file_name);

            var text_to_numeric_resolver = new TextToNumericResolver();
            var data_converted = text_to_numeric_resolver.GetDataFromText(data);

            var kologomorov = new KolmogorovReduction.KolmogorovReduction();

            SaveClassDistribution(data_file_name, data_converted);
            var kologomorov_measure = kologomorov.Compute(data_converted, data_file_name);

            foreach (var manager in test_managers)
            {
                manager.BeginTest(data_converted, kologomorov_measure, data_file_name);
            }
        }

        private static void SaveClassDistribution(string data_file_name, List<List<double>> data_converted)
        {
            var document = new Spreadsheet();
            var sheet = document.Workbook.Worksheets.Add("ClassDistribution");
            sheet.Cell("A1").Value = "L.p.";
            sheet.Cell("B1").Value = "Ilość danych dla klasy";
            sheet.Cell("C1").Value = "Rozkład klasy [%]";

            for (int i = 0; i < data_converted.GetClassificatorsCount(); i++)
            {
                sheet.Cell("A" + (2 + i)).Value = i;
                var class_count = data_converted.Count(x => (int)x.Last() == i);
                sheet.Cell("B" + (2 + i)).Value = class_count;
                sheet.Cell("C" + (2 + i)).Value = (float)class_count / data_converted.Count * 100;
            }
            data_converted.GetClassificatorsCount();

            document.SaveAs(DataPath.GetFilePath(data_file_name, "ClassDistribution"));
            document.Close();
        }
    }
}