﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms;
using Algorithms.Extenders;
using BenchmarkDotNet.Characteristics;
using Bytescout.Spreadsheet;

namespace Masters_degree_program
{
    public sealed class NearestMeanTestManager : ITestManager
    {
        private readonly bool m_normalize;
        public readonly List<DoubleCrossValidation> m_test_information = new List<DoubleCrossValidation>();

        public NearestMeanTestManager(bool normalize)
        {
            m_normalize = normalize;
        }
        public void BeginTest(List<List<double>> data, List<int> kologomorov_measure, string file_name)
        {
            var testing_data = m_normalize ? data.NormalizeFast() : data;

            var distance_measures = Enum.GetValues(typeof(DistanceMeasure));
            foreach (DistanceMeasure distance_measure in distance_measures)
            {
                for (int how_many_properties = 1;
                    how_many_properties < kologomorov_measure.Count;
                    how_many_properties++)
                {
                    var distance = new Distance(how_many_properties, kologomorov_measure, distance_measure);
                    var cross_validate = new DoubleCrossValidation();
                    m_test_information.Add(cross_validate);
                    var nearest_mean = new NearestMean(distance, testing_data.GetClassificatorsCount(),
                        testing_data.CountWithoutClassificator());
                    cross_validate.CrossValidate(testing_data, nearest_mean);
                }
            }

            SaveDataToExcel(file_name, SaveDetailedData, CommonFileNames.DETAILED_POSTFIX + GetNormalizePostfix());
            SaveDataToExcel(file_name, SaveData, "" + GetNormalizePostfix());
        }
        private string GetNormalizePostfix()
        {
            return m_normalize ? "Normalized" : "";
        }
        private void SaveData(Spreadsheet document)
        {
            Worksheet sheet = document.Workbook.Worksheets.Add("NearestMean");
            sheet.Cell("A5").Value = "Ilość własności";
            sheet.Cell("I2").Value = "Dystans";
            sheet.Cell("E1").Value = "Poprawne rozpoznania (średnia) [%]";
            sheet.Cell("A1").Value = "Normalizacja";
            sheet.Cell("A2").Value = m_normalize ? "Tak" : "Nie";
            foreach (DistanceMeasure distance in Enum.GetValues(typeof(DistanceMeasure)))
            {
                var the_same_distance_measure = m_test_information.Where(x => ((NearestMean)x.m_classificator).m_distance.m_distance_measure == distance).ToList();

                foreach (var single_test in the_same_distance_measure)
                {
                    var nearest_mean = single_test.m_classificator as NearestMean;

                    var how_many_properties = nearest_mean.m_distance.m_how_many_properties;

                    sheet.Cell(3, 3 + (int)nearest_mean.m_distance.m_distance_measure).Value = nearest_mean.m_distance.m_distance_measure.ToString();
                    sheet.Cell(how_many_properties + 3, 2).Value = how_many_properties;
                    sheet.Cell(3 + how_many_properties, 3 + (int)nearest_mean.m_distance.m_distance_measure).Value = single_test.AverageClassificationPercentage();
                }
            }
        }

        private void SaveDataToExcel(string file_name, Action<Spreadsheet> saving_function, string name_postfix)
        {
            var document = new Spreadsheet();
            saving_function(document);
            document.SaveAs(DataSaving.Common.DataPath.GetFilePath(file_name, "NearestMean" + name_postfix));
            document.Close();
        }

        private void SaveDetailedData(Spreadsheet document)
        {
            int spread_sheet_count = 0;
            foreach (var single_test in m_test_information)
            {
                Worksheet sheet = document.Workbook.Worksheets.Add("NearestMean" + spread_sheet_count.ToString());
                spread_sheet_count++;
                var nearest_mean = single_test.m_classificator as NearestMean;
                sheet.Cell("A1").Value = "Dystans";
                sheet.Cell("A2").Value = nearest_mean.m_distance.m_distance_measure;
                sheet.Cell("B1").Value = "Ilość własności";
                sheet.Cell("B2").Value = nearest_mean.m_distance.m_how_many_properties;
                sheet.Cell("C1").Value = "Normalizacja";
                sheet.Cell("C2").Value = m_normalize ? "Tak" : "Nie";

                sheet.Cell("A10").Value = "L.p.";
                sheet.Cell("B10").Value = "Poprawne rozpoznania [%]";
                for (int j = 0; j < single_test.m_classification_results.Count; j++)
                {
                    string cell = (11 + j).ToString();
                    sheet.Cell("A" + cell).Value = j + 1;
                    sheet.Cell("B" + cell).Value = single_test.AverageClassificationPercentageForOneInstance(j);
                }
            }
        }
    }
}