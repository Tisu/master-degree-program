﻿using System.Collections.Generic;

namespace Masters_degree_program
{
    public interface ITestManager
    {
        void BeginTest(List<List<double>> data, List<int> kologomorov_measure, string file_name);
    }
}