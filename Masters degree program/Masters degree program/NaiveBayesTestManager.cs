﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms;
using Algorithms.Classificators;
using Algorithms.Classificators.NaiveBayes;
using Algorithms.Extenders;
using Bytescout.Spreadsheet;

namespace Masters_degree_program
{
    public sealed class NaiveBayesTestManager : ITestManager
    {
        public readonly List<DoubleCrossValidation> m_test_information = new List<DoubleCrossValidation>();

        private readonly bool m_normalize;
        private NaiveBayesFactory m_testing_bayes_factory;
        public NaiveBayesTestManager(bool normalize)
        {
            m_normalize = normalize;
        }

        public void BeginTest(List<List<double>> data, List<int> kologomorov_measure, string file_name)
        {
            var normalized_data = m_normalize ? data.NormalizeFast() : data;

            for (int i = 1; i < kologomorov_measure.Count; i++)
            {
                var testing_data_reduced = normalized_data.ReduceKologomorov(kologomorov_measure, i);
                m_testing_bayes_factory = new NaiveBayesFactory(testing_data_reduced.CountWithoutClassificator(), testing_data_reduced.GetClassificatorsCount());
                foreach (var naive_bayes in m_testing_bayes_factory)
                {
                    var cross_validate = new DoubleCrossValidation();
                    try
                    {
                        cross_validate.CrossValidate(testing_data_reduced, naive_bayes);
                        m_test_information.Add(cross_validate);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            SaveDataToExcel(file_name, SaveDetailedData, CommonFileNames.DETAILED_POSTFIX + GetNormalizePostfix());
            SaveDataToExcel(file_name, SaveData, "" + GetNormalizePostfix());
        }

        private string GetNormalizePostfix()
        {
            return m_normalize ? "Normalized" : "";
        }
        private void SaveDataToExcel(string file_name, Action<Spreadsheet> saving_function, string name_postfix)
        {
            var document = new Spreadsheet();
            saving_function(document);
            document.SaveAs(DataSaving.Common.DataPath.GetFilePath(file_name, "NaiveBayes" + name_postfix));
            document.Close();
        }
        private void SaveData(Spreadsheet document)
        {
            SaveSingleTest(document,true);
            SaveSingleTest(document,false);
        }

        private void SaveSingleTest(Spreadsheet document, bool empirical)
        {
            Worksheet sheet = document.Workbook.Worksheets.Add("NaiveBayes" + empirical);
            sheet.Cell("A5").Value = "Ilość własności";
            sheet.Cell("E1").Value = "Poprawne rozpoznania (średnia) [%]";
            sheet.Cell("A1").Value = "Normalizacja";
            sheet.Cell("A2").Value = m_normalize ? "Tak" : "Nie";

            foreach (var single_test in m_test_information.Where(x => ((INaiveBayes) x.m_classificator).IsEmpirical == empirical))
            {
                if (!(single_test.m_classificator is INaiveBayes naive_bayes))
                {
                    return;
                }
                sheet.Cell("B1").Value = "Empiryczne";
                sheet.Cell("B2").Value = naive_bayes.Empirical;
                sheet.Cell("I2").Value = "Model";
                var row_shift = single_test.m_data.CountWithoutClassificator() - 1;
                var column_shift = m_testing_bayes_factory.m_naive_bayes_classificators.FindIndex(x =>
                {
                    var bayers = (INaiveBayes) x;
                    return bayers.Distribution == naive_bayes.Distribution;
                });
                if (column_shift == -1)
                {
                    throw new Exception("Not found");
                }
                sheet.Cell(2, 3 + column_shift).Value = naive_bayes.Distribution;
                sheet.Cell(row_shift + 3, 2).Value = single_test.m_data.CountWithoutClassificator();
                sheet.Cell(3 + row_shift, 3 + column_shift).Value = single_test.AverageClassificationPercentage();
            }
        }

        private void SaveDetailedData(Spreadsheet document)
        {
            int spread_sheet_count = 0;
            foreach (var single_test in m_test_information)
            {
                Worksheet sheet = document.Workbook.Worksheets.Add("NaiveBayes" + spread_sheet_count);
                spread_sheet_count++;

                var naive_bayes = (INaiveBayes)single_test.m_classificator;
                sheet.Cell("A1").Value = "Empiryczne";
                sheet.Cell("A2").Value = naive_bayes.Empirical;
                sheet.Cell("B1").Value = "Model";
                sheet.Cell("B2").Value = naive_bayes.Distribution;
                sheet.Cell("C1").Value = "Normalizacja";
                sheet.Cell("C2").Value = m_normalize ? "Tak" : "Nie"; ;

                sheet.Cell("A10").Value = "L.p.";
                sheet.Cell("B10").Value = "Poprawne rozpoznania";
                for (int j = 0; j < single_test.m_classification_results.Count; j++)
                {
                    string cell = (11 + j).ToString();
                    sheet.Cell("A" + cell).Value = j + 1;
                    sheet.Cell("B" + cell).Value = single_test.AverageClassificationPercentageForOneInstance(j);
                }
            }
        }
    }
}