﻿using System.Collections.Generic;

namespace Masters_degree_program
{
    public sealed class TestManagersFactory
    {
        public static List<ITestManager> GetTestManagers()
        {
            return new List<ITestManager>()
            {
                new KNearestNeighborsTestManager(true),
                new KNearestNeighborsTestManager(false),
                new NearestMeanTestManager(true),
                new NearestMeanTestManager(false),
                new NeuronNetworkTestManager(true),
                new NeuronNetworkTestManager(false),
                new NaiveBayesTestManager(true),
                new NaiveBayesTestManager(false),
            };
        }
    }
}