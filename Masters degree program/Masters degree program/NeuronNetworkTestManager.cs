﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Neuro.Learning;
using Algorithms;
using Algorithms.Classificators;
using Algorithms.Classificators.NaiveBayes;
using Algorithms.Classificators.Neuron;
using Algorithms.Extenders;
using Bytescout.Spreadsheet;

namespace Masters_degree_program
{
    class NeuronNetworkTestManager : ITestManager
    {
        private readonly bool m_normalize;
        public readonly List<DoubleCrossValidation> m_test_information = new List<DoubleCrossValidation>();

        public NeuronNetworkTestManager(bool normalize)
        {
            m_normalize = normalize;
        }

        public void BeginTest(List<List<double>> data, List<int> kologomorov_measure, string file_name)
        {
            var testing_data = m_normalize ? data.NormalizeFast() : data;
            var neuron_network_factory = new NeuronNetworkFactory();
            foreach (var neuron_network_classifier in neuron_network_factory)
            {
                var cross_validate = new DoubleCrossValidation();
                m_test_information.Add(cross_validate);
                cross_validate.CrossValidate(testing_data, neuron_network_classifier);
            }

            SaveDataToExcel(file_name, SaveDetailedData, CommonFileNames.DETAILED_POSTFIX + GetNormalizePostfix());
            SaveDataToExcel(file_name, SaveData, GetNormalizePostfix());
        }
        private string GetNormalizePostfix()
        {
            return m_normalize ? "Normalized" : string.Empty;
        }
        private void SaveData(Spreadsheet document)
        {
            SaveRegularization(document, true);
            SaveRegularization(document, false);
        }

        private void SaveRegularization(Spreadsheet document, bool regularization)
        {
           Worksheet sheet = document.Workbook.Worksheets.Add("NeuronNetworkReg" + regularization);
            sheet.Cell("A1").Value = "Sieć neuronowa";
            sheet.Cell("B1").Value = "Poprawne rozpoznania [%]";
            sheet.Cell("C1").Value = "Normalizacja";
            sheet.Cell("C2").Value = m_normalize ? "Tak" : "Nie";
            foreach (var single_test in m_test_information.Where(x => ((LevenbergMarquardtLearning)((INeuronNetworkWrapper)x.m_classificator).m_teacher).UseRegularization == regularization))
            {
                var neuron_layer_index = 0;
                var activation_function_index = 0;
                if (single_test.m_classificator is INeuronNetworkWrapper neuron_network)
                {
                    var neuron_layer = "";
                    for (int i = 0; i < neuron_network.m_activation_network.Layers.Length; i++)
                    {
                        neuron_layer += neuron_network.m_activation_network.Layers[i].Neurons.Length.ToString();
                        if (i != neuron_network.m_activation_network.Layers.Length - 1)
                        {
                            neuron_layer += ",";
                        }
                    }
                    neuron_layer_index = NeuronNetworkFactory.m_neuron_layers.IndexOf(neuron_network.m_neuron_layer_count);
                    activation_function_index =
                        NeuronNetworkFactory.m_activation_functions.IndexOf(neuron_network.m_activation_function);
                    sheet.Cell("B3").Value = "Funkcja aktywacyjna \\  warstwy neuronów";
                    sheet.Cell((char)('C' + neuron_layer_index) + "3").Value = neuron_layer;
                    sheet.Cell("B" + (4 + activation_function_index)).Value = neuron_network.m_activation_function.ToString();
                }
                sheet.Cell((char)('C' + neuron_layer_index) + (4 + activation_function_index).ToString()).Value =
                    single_test.AverageClassificationPercentage();
            }
        }

        private void SaveDataToExcel(string file_name, Action<Spreadsheet> saving_function, string name_postfix)
        {
            var document = new Spreadsheet();
            saving_function(document);
            document.SaveAs(DataSaving.Common.DataPath.GetFilePath(file_name, "NeuronNetwork" + name_postfix));
            document.Close();
        }

        private void SaveDetailedData(Spreadsheet document)
        {
            int spread_sheet_count = 0;
            foreach (var single_test in m_test_information)
            {
                Worksheet sheet = document.Workbook.Worksheets.Add("NeuronNetwork" + spread_sheet_count.ToString());
                spread_sheet_count++;

                if (single_test.m_classificator is INeuronNetworkWrapper neuron_network)
                {
                    sheet.Cell("A1").Value = "Sieć neuronowa";
                    var neuron_layer = "";
                    for (int i = 0; i < neuron_network.m_activation_network.Layers.Length; i++)
                    {
                        neuron_layer += neuron_network.m_activation_network.Layers[i].Neurons.Length.ToString();
                        if (i != neuron_network.m_activation_network.Layers.Length - 1)
                        {
                            neuron_layer += ",";
                        }
                    }

                    sheet.Cell("A2").Value = neuron_layer;
                    sheet.Cell("B1").Value = "Funkcja aktywacyjna";
                    sheet.Cell("B2").Value = neuron_network.m_activation_function.ToString();
                }
                sheet.Cell("C1").Value = "Normalizacja";
                sheet.Cell("C2").Value = m_normalize ? "Tak" : "Nie";

                sheet.Cell("A10").Value = "L.p.";
                sheet.Cell("B10").Value = "Poprawne rozpoznania [%]";
                for (int j = 0; j < single_test.m_classification_results.Count; j++)
                {
                    string cell = (11 + j).ToString();
                    sheet.Cell("A" + cell).Value = j + 1;
                    sheet.Cell("B" + cell).Value = single_test.AverageClassificationPercentageForOneInstance(j);
                }
            }
        }
    }
}
