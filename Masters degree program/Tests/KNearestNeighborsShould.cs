﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using Algorithms;
using Algorithms.Extenders;
using FileDataReader;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class KNearestNeighborsShould
    {
        [Test]
        public void KologomorovReductionExtenderShouldReturnLessData()
        {
            var test1 = new List<List<double>>()
            {
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},
                new List<double>(){1,2,3,4,5,6,7,8,9},

            };
            var kologomorov_reduction = new List<int> { 8, 7, 6, 5, 4, 3, 2, 1, 0 };
            var new_list_size = 5;
            var result1 = test1.ReduceKologomorov(kologomorov_reduction, new_list_size);

            //classificator is last
            Assert.AreEqual(result1.First().Count, new_list_size + 1);
            for (int i = 0; i < result1.CountWithoutClassificator(); i++)
            {
                Assert.AreEqual(9 - i, result1.First()[i]);
            }
        }
        
        [Test]
        public void SpeedTest()
        {
            var test = new FileDataReader.FileDataReader();
            var data = test.GetDataFromFile(@"D:\GIT\Masters degree program\Data\data.txt");

            var converted_data = new TextToNumericResolver();
            var data_converted = converted_data.GetDataFromText(data);


            var test1 = new FileDataReader.FileDataReader();
            var data1 = test.GetDataFromFile(@"D:\GIT\Masters degree program\Data\data.txt");

            var converted_data1 = new TextToNumericResolver();
            var data_converted1 = converted_data.GetDataFromText(data);

            var timer = new Stopwatch();

            timer.Start();
            data_converted1.NormalizeFast();
            timer.Stop();

            var second = timer.ElapsedMilliseconds;

            timer.Reset();

            timer.Start();
            data_converted.Normalize();
            timer.Stop();

            var first = timer.ElapsedMilliseconds;

            Assert.Greater(first, second);
        }
    }
}
