﻿using System.IO;

namespace DataSaving.Common
{
    public static class DataPath
    {
        public const string DATA_FOLDER = @"..\..\DataOutput\";

        public static string GetFilePath(string file_name, string method_name)
        {
            if(!Directory.Exists(DATA_FOLDER))
            {
                Directory.CreateDirectory(DATA_FOLDER);
            }

            var subfolder = DATA_FOLDER + "/" + Path.GetFileNameWithoutExtension(file_name) + "/";
            if(!Directory.Exists(subfolder))
            {
                Directory.CreateDirectory(subfolder);
            }
            if(File.Exists(subfolder + method_name + ".xls"))
            {
                throw new FileNotFoundException("Backup data before computing");
            }
            return subfolder + method_name + ".xls";
        }
    }
}