﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bytescout.Spreadsheet;
using DataSaving.Common;

namespace KolmogorovReduction
{
    public class KolmogorovReduction
    {
        public List<int> Compute(List<List<double>> patient_data, string file_name)
        {
            var claas_information = new List<OneClassInformation>();
            var unique_class_information = new List<UniqueClassInformation>();
            var max = patient_data.Max(x => x.LastOrDefault());
            var min = patient_data.Min(x => x.LastOrDefault());
            var sum = patient_data.Count;
            for(var i = min; i <= max; i++)
            {
                var one_class_info = new OneClassInformation(patient_data.Where(x => x.LastOrDefault() == i).ToList());
                claas_information.Add(one_class_info);

                var unique_class_info = new UniqueClassInformation(patient_data.Where(x => x.LastOrDefault() == i).ToList());
                unique_class_information.Add(unique_class_info);

                var clasificator = new Classificator();
                clasificator.m_a_priori_probability = patient_data.Select(x => x.LastOrDefault()).Count(x => x == i) / (float)sum;
                one_class_info.m_classificator = clasificator;
            }
            return Probability(claas_information, unique_class_information, patient_data, file_name);
        }

        private List<int> Probability(
            List<OneClassInformation> one_class_informations,
            List<UniqueClassInformation> unique_class_information,
            List<List<double>> patient_data,
            string file_name)
        {
            Dictionary<int, float> result = new Dictionary<int, float>();
            float measure_per_class = 0;
            for(int property_index = 0; property_index < patient_data.FirstOrDefault().Count - 1; property_index++)
            {
                result.Add(property_index, 0);
                for(int class_index = 0; class_index <= patient_data.Max(x => x.LastOrDefault()); class_index++)
                {
                    measure_per_class = 0;
                    var unique = unique_class_information[class_index].GetUniqueValuesByIndex(property_index);
                    var one_class_info = one_class_informations[class_index];
                    foreach(var unique_value in unique)
                    {
                        measure_per_class += Math.Abs(
                           one_class_info.GetProbabilityOfVariable(property_index, unique_value) -
                           CalculatePropabilityInOtherClasses(property_index, unique_value, one_class_informations));
                    }
                    measure_per_class *= one_class_informations.ElementAt(class_index).m_classificator.m_a_priori_probability;
                    result[property_index] += measure_per_class;
                }
            }

            var document = new Spreadsheet();
            var sheet = document.Workbook.Worksheets.Add("KologomorovMeasure");
            sheet.Cell("A1").Value = "L.p.";
            sheet.Cell("B1").Value = "Miara Kologomorova";

            for(int i = 0; i < result.Count; i++)
            {
                string cell = (2 + i).ToString();
                sheet.Cell("A" + cell).Value = i + 1;
                sheet.Cell("B" + cell).Value = Math.Round(result[i],2);
            }

            document.SaveAs(DataPath.GetFilePath(file_name, "KologomorovMeasure"));
            document.Close();

            return result.OrderByDescending(x => x.Value).Select(x => x.Key).ToList();
        }

        private float CalculatePropabilityInOtherClasses(int property_index, double value, List<OneClassInformation> one_class_informations)
        {
            float result = 0;
            foreach(var one_clas in one_class_informations)
            {
                result += one_clas.GetProbabilityOfVariable(property_index, value) * one_clas.m_classificator.m_a_priori_probability;
            }
            return result;
        }
    }
}