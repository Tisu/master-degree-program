﻿using System.Collections.Generic;

namespace KolmogorovReduction
{
    public class UniqueClassInformation
    {
        readonly List<HashSet<double>> m_propability_list = new List<HashSet<double>>();

        public UniqueClassInformation(List<List<double>> patient_data)
        {
            for(int i = 0; i < patient_data[0].Count; i++)
            {
                var set = new HashSet<double>();
                m_propability_list.Add(set);

                foreach(var single_patient in patient_data)
                {
                    set.Add(single_patient[i]);
                }
            }
        }

        public HashSet<double> GetUniqueValuesByIndex(int index)
        {
            return m_propability_list[index];
        }
    }
}