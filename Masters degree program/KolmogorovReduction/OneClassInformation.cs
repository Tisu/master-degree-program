﻿using System.Collections.Generic;

namespace KolmogorovReduction
{
    public class OneClassInformation
    {
        public Classificator m_classificator;
        readonly List<Dictionary<double, float>> m_propability_list = new List<Dictionary<double, float>>();

        public OneClassInformation(List<List<double>> patient_data)
        {
            CalculatePropability(patient_data);
        }
        public void CalculatePropability(List<List<double>> patient_data)
        {
            for(int patient_data_index = 0; patient_data_index < patient_data[0].Count; patient_data_index++)
            {
                m_propability_list.Add(new Dictionary<double, float>());
                var dictionary = m_propability_list[patient_data_index];
                foreach (var patient in patient_data)
                {
                    var value = patient[patient_data_index];
                    if(!dictionary.ContainsKey(value))
                    {
                        dictionary[value] = 1.0f / patient_data.Count;
                    }
                    else
                    {
                        dictionary[value] += 1.0f / patient_data.Count;
                    }
                }
            }
        }

        public float GetProbabilityOfVariable(int variable_index, double value)
        {
            var dictionary = m_propability_list[variable_index];
            if(dictionary.ContainsKey(value))
            {
                return dictionary[value];
            }
            return 0;
        }
    }
}