﻿using System;
using System.Collections.Generic;

namespace Algorithms
{
    public class Distance
    {
        public readonly int m_how_many_properties;
        public readonly List<int> m_kologomorov_measure;
        public readonly Func<List<double>, List<double>, double> m_distance_function;
        public DistanceMeasure m_distance_measure;
        public Distance(int how_many_properties, List<int> kologomorov_measure, DistanceMeasure measure)
        {
            if (how_many_properties == kologomorov_measure.Count)
            {
                throw new ArgumentOutOfRangeException($"{nameof(how_many_properties)} must be less than {nameof(kologomorov_measure)}");
            }
            if (how_many_properties <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(how_many_properties)} must be more than 0");
            }
            m_kologomorov_measure = kologomorov_measure;
            m_how_many_properties = how_many_properties;
            m_distance_measure = measure;
            switch(measure)
            {
                case DistanceMeasure.euclides:
                    m_distance_function = CalculateDistanceEuclides;
                    break;
                case DistanceMeasure.manhatan:
                    m_distance_function = CalculateDistanceManhatan;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(measure), measure, null);
            }
        }

        public double CalculateDistanceEuclides(List<double> a_data1, List<double> a_data2)
        {
            double distance = 0;
            for(int i = 0; i < m_how_many_properties; i++)
            {
                var property_index = m_kologomorov_measure[i];
                distance += Math.Pow(a_data1[property_index] - a_data2[property_index], 2);
            }
            return Math.Sqrt(distance);
        }
        public double CalculateDistanceManhatan(List<double> a_data1, List<double> a_data2)
        {
            double distance = 0;
            for(int i = 0; i < m_how_many_properties; i++)
            {
                var property_index = m_kologomorov_measure[i];
                distance += Math.Abs(a_data1[property_index] - a_data2[property_index]);
            }
            return distance;
        }
    }
}
