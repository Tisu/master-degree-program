﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms
{
    public class Survey
    {
        public List<int> NewSelection(int quantity_of_patient)
        {
            var mix_of_values = new List<KeyValuePair<int, int>>();
            Random random = new Random();
            for(int i = 0; i < quantity_of_patient; i++)
            {
                mix_of_values.Add(new KeyValuePair<int, int>(i, random.Next(4000)));
            }
            return mix_of_values.OrderBy(x => x.Value).Select(x => x.Key).ToList();
        }
    }
}