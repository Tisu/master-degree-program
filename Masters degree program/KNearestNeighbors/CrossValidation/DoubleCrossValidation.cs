﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Extenders;

namespace Algorithms
{
    public class DoubleCrossValidation
    {
        const int HOW_MANY_TIMES = 5;
        public readonly List<int> m_classification_results = new List<int>();
        public List<List<double>> m_data;
        public IClassificator m_classificator;

        public double AverageClassificationPercentage()
        {
            return m_classification_results.Average() / (m_data.Count / 2) * 100;
        }
        public double AverageClassificationPercentageForOneInstance(int index)
        {
            return ((double)m_classification_results[index]) / (m_data.Count / 2) * 100;
        }
        public void CrossValidate(List<List<double>> data, IClassificator classificator)
        {
            m_data = data;

            var survey = new Survey();
            m_classificator = classificator;

            for (int i = 0; i < HOW_MANY_TIMES; i++)
            {
                var new_random_indexes = survey.NewSelection(data.Count);

                classificator.Learn(data.GetFirstHalfWithIndexes(new_random_indexes));
                var result = classificator.Compute(data.GetSecondHalfWithIndexes(new_random_indexes));
                m_classification_results.Add(result);

                classificator.Learn(data.GetSecondHalfWithIndexes(new_random_indexes));
                result = classificator.Compute(data.GetFirstHalfWithIndexes(new_random_indexes));
                m_classification_results.Add(result);
            }
        }
    }
}

