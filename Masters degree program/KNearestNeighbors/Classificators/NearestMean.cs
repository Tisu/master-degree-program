﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms.Extenders;

namespace Algorithms
{
    public class NearestMean : IClassificator
    {
        public readonly Dictionary<int, double[]> m_mean_data_classificator = new Dictionary<int, double[]>();
        public readonly Distance m_distance;

        public NearestMean(Distance distance, int means, int data_without_classificator_length)
        {
            m_distance = distance;
            for(var i = 0; i <= means; i++)
            {
                var mean_patient_data = new double[data_without_classificator_length];
                m_mean_data_classificator.Add(i, mean_patient_data);
            }
        }

        public int Compute(List<List<double>> data)
        {
            var good_recognition = 0;

            for(int i = 0; i < data.Count; i++)
            {
                var result = Classificate(data[i]);
                if(data.GetClassficator(i) == result)
                {
                    ++good_recognition;
                }
            }
            return good_recognition;
        }
        public void Learn(List<List<double>> patient_data)
        {
            var list = new List<double>();
            for(int i = 0; i < m_mean_data_classificator.Count; i++)
            {
                for(int property = 0; property < m_distance.m_how_many_properties; property++)
                {
                    var property_index = m_distance.m_kologomorov_measure[property];
                    for(int j = 0; j < patient_data.Count; j++)
                    {
                        if(patient_data.GetClassficator(j) != i)
                        {
                            continue;
                        }

                        list.Add(patient_data[j][property_index]);
                    }
                    if(list.Count > 0)
                    {
                        var average = list.Average();
                        double[] patient_data_cache;
                        if(m_mean_data_classificator.TryGetValue(i, out patient_data_cache))
                        {
                            patient_data_cache[property_index] = average;
                        }
                        list.Clear();
                    }
                }
            }
        }

        private int Classificate(List<double> patient_data)
        {
            var distance_results = new List<KeyValuePair<int, double>>();
            foreach(var clasificator in m_mean_data_classificator)
            {
                distance_results.Add(new KeyValuePair<int, double>(clasificator.Key, m_distance.m_distance_function(clasificator.Value.ToList(), patient_data)));
            }
            var min = distance_results.OrderBy(x => x.Value).FirstOrDefault();
            return min.Key;
        }
    }
}
