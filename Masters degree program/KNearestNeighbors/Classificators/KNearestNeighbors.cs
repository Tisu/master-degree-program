﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Extenders;

namespace Algorithms
{
    public class KNearestNeighbors : IClassificator
    {
        public readonly int m_k;
        public readonly Distance m_distance;
        private List<List<double>> m_learning_set;
        public KNearestNeighbors(int k, Distance distance)
        {
            m_k = k;
            m_distance = distance;
        }
        public int Compute(List<List<double>> data)
        {
            var good_recognition = 0;

            for(int i = 0; i < data.Count; i++)
            {
                var result = Classificate(data[i]);                
                if(data.GetClassficator(i) == result)
                {
                    ++good_recognition;
                }
            }
            return good_recognition;
        }
        
        public void Learn(List<List<double>> data)
        {
            m_learning_set = data;
        }
        private int Classificate(List<double> doubles)
        {
            var distances = new List<KeyValuePair<int, double>>();
            for(int i = 0; i < m_learning_set.Count; i++)
            {
                var distance = m_distance.m_distance_function(doubles, m_learning_set[i]);
                distances.Add(new KeyValuePair<int, double>(m_learning_set.GetClassficator(i), distance));
            }
            return distances.OrderBy(x => x.Value).Take(m_k).GroupBy(x => x.Key).OrderByDescending(x => x.Count()).First().Key;
        }
    }
}
