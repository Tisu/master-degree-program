﻿using System.Collections.Generic;

namespace Algorithms.Classificators.NaiveBayes
{
    public interface INaiveBayes
    {
        string Distribution { get; }
        string Empirical { get; }
        bool IsEmpirical { get;}
        int Compute(List<List<double>> data);
        void Learn(List<List<double>> data);
    }
}