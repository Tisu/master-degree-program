﻿using System;
using System.Collections;
using System.Collections.Generic;
using Accord.MachineLearning.Bayes;
using Accord.Statistics.Distributions.Univariate;

namespace Algorithms.Classificators.NaiveBayes
{
    public sealed class NaiveBayesFactory : IEnumerable<IClassificator>
    {
        private readonly int m_data_without_classificator;
        private readonly int m_classificators_count;
        public List<IClassificator> m_naive_bayes_classificators;

        public NaiveBayesFactory(int data_without_classificator, int classificators_count)
        {
            m_data_without_classificator = data_without_classificator;
            m_classificators_count = classificators_count;
            try
            {
                m_naive_bayes_classificators = GetClassificators();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private List<IClassificator> GetClassificators()
        {
            return new List<IClassificator>
            {
                new NaiveBayesGenericWrapper<NormalDistribution>(new NaiveBayesLearning<NormalDistribution>()
                {
                    Model = new NaiveBayes<NormalDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: NormalDistribution.Standard),
                    Empirical = true,
                }),
               
                new NaiveBayesGenericWrapper<UniformDiscreteDistribution>(
                    new NaiveBayesLearning<UniformDiscreteDistribution>()
                    {
                        Model = new NaiveBayes<UniformDiscreteDistribution>(inputs: m_data_without_classificator,
                            classes: m_classificators_count, initial: new UniformDiscreteDistribution(0, 3)),
                        Empirical = true,
                    }),
                
                new NaiveBayesGenericWrapper<CauchyDistribution>(new NaiveBayesLearning<CauchyDistribution>()
                {
                    Model = new NaiveBayes<CauchyDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: new CauchyDistribution()),
                    Empirical = true,
                }),
                
                new NaiveBayesGenericWrapper<PoissonDistribution>(new NaiveBayesLearning<PoissonDistribution>()
                {
                    Model = new NaiveBayes<PoissonDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: new PoissonDistribution()),
                    Empirical = true,
                }),
               
                new NaiveBayesGenericWrapper<LognormalDistribution>(new NaiveBayesLearning<LognormalDistribution>()
                {
                    Model = new NaiveBayes<LognormalDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: new LognormalDistribution()),
                    Empirical = true,
                }),

               

                new NaiveBayesGenericWrapper<NormalDistribution>(new NaiveBayesLearning<NormalDistribution>()
                {
                    Model = new NaiveBayes<NormalDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: NormalDistribution.Standard),
                    Empirical = false,
                }),

                new NaiveBayesGenericWrapper<UniformDiscreteDistribution>(
                    new NaiveBayesLearning<UniformDiscreteDistribution>()
                    {
                        Model = new NaiveBayes<UniformDiscreteDistribution>(inputs: m_data_without_classificator,
                            classes: m_classificators_count, initial: new UniformDiscreteDistribution(0, 3)),
                        Empirical = false,
                    }),
                new NaiveBayesGenericWrapper<CauchyDistribution>(new NaiveBayesLearning<CauchyDistribution>()
                {
                    Model = new NaiveBayes<CauchyDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: new CauchyDistribution()),
                    Empirical = false,
                }),
                new NaiveBayesGenericWrapper<PoissonDistribution>(new NaiveBayesLearning<PoissonDistribution>()
                {
                    Model = new NaiveBayes<PoissonDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: new PoissonDistribution()),
                    Empirical = false,
                }),

                new NaiveBayesGenericWrapper<LognormalDistribution>(new NaiveBayesLearning<LognormalDistribution>()
                {
                    Model = new NaiveBayes<LognormalDistribution>(inputs: m_data_without_classificator,
                        classes: m_classificators_count, initial: new LognormalDistribution()),
                    Empirical = false,
                }),
            };
        }
        public IEnumerator<IClassificator> GetEnumerator()
        {
            foreach (var naive_bayes in m_naive_bayes_classificators)
            {
                yield return naive_bayes;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}