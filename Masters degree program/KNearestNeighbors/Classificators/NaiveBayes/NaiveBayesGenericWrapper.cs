﻿using System;
using System.Collections.Generic;
using Accord.MachineLearning.Bayes;
using Accord.Statistics.Distributions;
using Accord.Statistics.Distributions.Univariate;
using Algorithms.Extenders;

namespace Algorithms.Classificators.NaiveBayes
{
    public sealed class NaiveBayesGenericWrapper<TAccordDistribution> : IClassificator, INaiveBayes where TAccordDistribution : IFittableDistribution<double>, IUnivariateDistribution<double>, IUnivariateDistribution
    {
        public NaiveBayesLearning<TAccordDistribution> m_bayes_learning;
        public string Empirical => m_bayes_learning.Empirical.ToString();
        public string Distribution => m_bayes_learning.Distribution.ToString().Substring(m_bayes_learning.Distribution.ToString().LastIndexOf(".", StringComparison.Ordinal) + 1);
        public bool IsEmpirical => m_bayes_learning.Empirical;

        public NaiveBayesGenericWrapper(NaiveBayesLearning<TAccordDistribution> learning_bayes)
        {
            m_bayes_learning = learning_bayes;
        }
        public int Compute(List<List<double>> data)
        {
            var good_recognition = 0;
            for (int i = 0; i < data.Count; i++)
            {
                var answer = m_bayes_learning.Model.Decide(data.GetComputeInputDouble(i));
                var should = data.GetClassficator(i);
                if (answer == should)
                {
                    good_recognition++;
                }
            }
            return good_recognition;
        }

        public void Learn(List<List<double>> data)
        {
            var inputs = data.GetLearningInputsDouble();
            var outputs = data.GetOutputs();
            m_bayes_learning.Learn(inputs, outputs);
        }
    }
}