﻿using Accord.Neuro;
using Accord.Neuro.Learning;

namespace Algorithms.Classificators
{
    public interface INeuronNetworkWrapper
    {
        ActivationNetwork m_activation_network { get; }
        IActivationFunction m_activation_function { get; }
        int[] m_neuron_layer_count { get; }
        ISupervisedLearning m_teacher{ get; }
    }
}