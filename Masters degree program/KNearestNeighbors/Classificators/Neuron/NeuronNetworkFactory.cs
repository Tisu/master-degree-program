﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Accord.Neuro;

namespace Algorithms.Classificators.Neuron
{
    public sealed class NeuronNetworkFactory : IEnumerable<IClassificator>
    {
        private List<IClassificator> m_classificators => GenerateClassificators();
        public static List<IActivationFunction> m_activation_functions = new List<IActivationFunction>()
        {
            new BipolarSigmoidFunction(),
            new IdentityFunction(),
            new LinearFunction(),
            new SigmoidFunction(),
            new RectifiedLinearFunction(),
            new ThresholdFunction(),
        };

        public static List<int[]> m_neuron_layers = new List<int[]>()
        {
            new [] {10,8,5,3,1},
            new [] {7,10,7,5,1},
            new [] {10,8,5,1},
            new [] {10,5,1},
            new [] {7,5,1},
            new [] {5,10,1},
            new [] {5,5,1},
            new [] {5,3,1},
            new [] {10,1},
            new [] {7,1},
            new [] {5,1},
            new [] {3,1},
            new [] {1},
        };
        private List<IClassificator> GenerateClassificators()
        {
            var generated_classificators = new List<IClassificator>();

            foreach (var function in m_activation_functions)
            {
                generated_classificators.AddRange(m_neuron_layers.Select(layer => new NeuronNetworkWrapper(function, layer, true)));
                generated_classificators.AddRange(m_neuron_layers.Select(layer => new NeuronNetworkWrapper(function, layer, false)));
            }
            return generated_classificators;
        }

        public IEnumerator<IClassificator> GetEnumerator()
        {
            foreach (var classificator in m_classificators)
            {
                yield return classificator;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}