﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Accord;
using Accord.Math;
using Accord.Neuro;
using Accord.Neuro.ActivationFunctions;
using Accord.Neuro.Learning;
using Accord.Neuro.Networks;
using Accord.Statistics.Testing;
using Algorithms.Extenders;

namespace Algorithms.Classificators
{
    public class NeuronNetworkWrapper : IClassificator, INeuronNetworkWrapper
    {
        private  const int MAX_NEURON_LEARNING_ITERATION = 50;
        public IActivationFunction m_activation_function { get; }
        public int[] m_neuron_layer_count { get; }
        public ActivationNetwork m_activation_network { get; set; }

        public ISupervisedLearning m_teacher { get; private set; }
        private readonly bool m_use_regularization;

        public NeuronNetworkWrapper(IActivationFunction activation_function, int[] neuron_count, bool use_regularization)
        {
            m_activation_function = activation_function;
            m_neuron_layer_count = neuron_count;
            m_use_regularization = use_regularization;
        }
        public int Compute(List<List<double>> data)
        {
            var good_recognision = 0;

            for (int i = 0; i < data.Count; i++)
            {
                var result = m_activation_network.Compute(data.GetComputeInputDouble(i));
                var classification = (int)(result[0] + 0.5);
                if (classification == data.GetClassficator(i))
                {
                    ++good_recognision;
                }
            }

            return good_recognision;
        }

        public void Learn(List<List<double>> data)
        {
            m_activation_network = new ActivationNetwork(m_activation_function, data.CountWithoutClassificator(), m_neuron_layer_count);

            m_teacher = new LevenbergMarquardtLearning(m_activation_network)
            {
                UseRegularization = m_use_regularization
            };
            var learning_data = data.GetLearningInputsDouble();
            var output_data = data.GetNeuroOutputsDouble();

            double error = double.MaxValue;
            double previous;
            var iteration = 0;
            do
            {
                if (iteration >= MAX_NEURON_LEARNING_ITERATION)
                {
                    break;
                }

                previous = error;
                error = m_teacher.RunEpoch(learning_data, output_data);
                iteration++;
            } while (Math.Abs(previous - error) > 0.01f );
        }
    }
}
