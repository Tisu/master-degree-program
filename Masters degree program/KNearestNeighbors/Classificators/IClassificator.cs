﻿using System.Collections.Generic;

namespace Algorithms
{
    public interface IClassificator
    {
        int Compute(List<List<double>> data);
        void Learn(List<List<double>> patient_data);
    }
}