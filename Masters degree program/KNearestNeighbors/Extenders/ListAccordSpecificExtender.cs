﻿using System.Collections.Generic;
using System.Linq;

namespace Algorithms.Extenders
{
    public static class ListAccordSpecificExtender
    {
        public static int[][] GetLearningInputs(this List<List<double>> list)
        {
            return list.Select(x => x.Select(y => (int)y).Take(list.CountWithoutClassificator()).ToArray()).ToArray();
        }
        public static int[] GetComputeInput(this List<List<double>> list,int index)
        {
            return list[index].Select(y => (int)y).Take(list.CountWithoutClassificator()).ToArray();
        }

        public static int[] GetOutputs(this List<List<double>> list)
        {
            return list.Select(x => (int)x.Last()).ToArray();
        }
        public static double[][] GetLearningInputsDouble(this List<List<double>> list)
        {
            return list.Select(x => x.Take(list.CountWithoutClassificator()).ToArray()).ToArray();
        }
        public static double[] GetComputeInputDouble(this List<List<double>> list, int index)
        {
            return list[index].Take(list.CountWithoutClassificator()).ToArray();
        }

        public static double[] GetOutputsDouble(this List<List<double>> list)
        {
            return list.Select(x => x.Last()).ToArray();
        }
        public static double[][] GetNeuroOutputsDouble(this List<List<double>> list)
        {
            return list.Select(x => x.Skip(list.CountWithoutClassificator()).ToArray()).ToArray();
        }
    }
}