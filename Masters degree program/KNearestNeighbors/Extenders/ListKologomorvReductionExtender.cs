﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms.Extenders
{
    public static class ListKologomorvReductionExtender
    {
        public static List<List<double>> ReduceKologomorov(
            this List<List<double>> data, 
            List<int> kologomorov_measure,
            int how_many_properties)
        {
            return data.Select(x => x.GetListWithIndexesAndClassificator(kologomorov_measure.Take(how_many_properties).ToList())).ToList();
        }

        public static List<double> GetListWithIndexesAndClassificator(this List<double> data, List<int> indexes)
        {
            if (data.Count <= 1)
            {
                throw new ArgumentException("Data should have at least 2 arguments, last one is classificator");
            }
            var new_data = new List<double>();
            for (var i = 0; i < indexes.Count; i++)
            {
                new_data.Add(data[indexes[i]]);
            }
            new_data.Add(data.Last());

            return new_data;
        }
    }
}