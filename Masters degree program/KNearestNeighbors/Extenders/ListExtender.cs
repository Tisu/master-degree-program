﻿using System.Collections.Generic;
using System.Linq;
using Accord.Math;

namespace Algorithms.Extenders
{
    public static class ListExtender
    {
        public static int CountWithoutClassificator(this List<List<double>> patient_data)
        {
            return patient_data.First().Count - 1;
        }
        public static int GetClassficator(this List<List<double>> patient_data, int index)
        {
            return (int)patient_data[index].LastOrDefault();
        }
        public static int GetClassificatorsCount(this List<List<double>> list)
        {
            return (int)list.Select(x => x.Last()).Max() + 1;
        }
        public static List<T> GetFirstHalf<T>(this List<T> list)
        {
            return list.Take(list.Count / 2).ToList();
        }
        public static List<T> GetSecondHalf<T>(this List<T> list)
        {
            return list.Skip(list.Count / 2).ToList();
        }
        public static List<T> GetListWithIndexes<T>(this List<T> list, List<int> indexes)
        {
            return list.Get(indexes.ToArray());
        }
        public static List<T> GetFirstHalfWithIndexes<T>(this List<T> list, List<int> indexes)
        {
            return list.Get(indexes.GetFirstHalf().ToArray());
        }
        public static List<T> GetSecondHalfWithIndexes<T>(this List<T> list, List<int> indexes)
        {
            return list.Get(indexes.GetSecondHalf().ToArray());
        }

        public static List<List<double>> Normalize(this List<List<double>> list)
        {
            var normalized_list = list.DeepCopy();
            for(int i = 0; i < list.First().Count; i++)
            {
                double max_value = list.Select(x => x.ElementAt(i)).Max();
                double min_value = list.Select(x => x.ElementAt(i)).Min();

                foreach(var record in normalized_list)
                {
                    record[i] = (record[i] - min_value) / (max_value - min_value);
                }
            }
            return normalized_list;
        }

        public static List<List<double>> NormalizeFast(this List<List<double>> list)
        {
            var normalized_list = list.DeepCopy();

            for(int i = 0; i < list[0].Count; i++)
            {
                double max_value = 0;
                double min_value = double.MaxValue;
                for(int j = 0; j < list.Count; j++)
                {
                    var current_value = list[j][i];
                    if(current_value > max_value)
                    {
                        max_value = current_value;
                    }
                    if(current_value < min_value)
                    {
                        min_value = current_value;
                    }
                }

                for(int k = 0; k < list.Count; k++)
                {
                    normalized_list[k][i] = (list[k][i] - min_value) / (max_value - min_value);
                }
            }
            return normalized_list;
        }

        public static List<List<double>> DeepCopy(this List<List<double>> list)
        {
            List<List<double>> copied_list = new List<List<double>>(list.Capacity);

            for(int j = 0; j < list.Count; j++)
            {
                copied_list.Add(new List<double>());
                copied_list[j].Capacity = list[j].Capacity;
                for(int i = 0; i < list.First().Count; i++)
                {
                    copied_list[j].Add(list[j][i]);
                }
            }
            return copied_list;
        }
    }
}
