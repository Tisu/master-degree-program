﻿using System.Collections.Generic;

namespace FileDataReader
{
    public static class HashSetExtender
    {
        public static int GetIndex(this HashSet<string> datas, string compare_to)
        {
            var i = 0;
            foreach(var data in datas)
            {
                if(data == compare_to)
                {
                    return i;
                }
                i++;
            }
            return i;
        }
    }
}