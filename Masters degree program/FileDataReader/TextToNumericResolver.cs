﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;

namespace FileDataReader
{
    public sealed class TextToNumericResolver
    {
        private const string UNKNOWN_ATTRIBUTE = "?";
        public List<List<double>> GetDataFromText(List<List<string>> data)
        {
            var result = new List<List<double>>();
            for (var i = 0; i < data.Count; i++)
            {
                var line = new List<double>();
                for (int j = 0; j < data[0].Count; j++)
                {
                    line.Add(0);
                }
                result.Add(line);
            }

            double buffer;
            for(var j = 0; j < data[0].Count; j++)
            {
                var unique = new HashSet<string>();
                for(var i = 0; i < data.Count; i++)
                {
                    var single_data = data[i][j];
                    if (double.TryParse(single_data, NumberStyles.Any, CultureInfo.InvariantCulture, out buffer))
                    {
                        result[i][j] = buffer;
                        continue;
                    }
                    if (unique.Contains(single_data))
                    {
                        result[i][j] = unique.GetIndex(single_data);
                        continue;
                    }
                    if (single_data == UNKNOWN_ATTRIBUTE)
                    {
                        throw new ArithmeticException("Wrong data delete unknown parameters");
                    }
                    result[i][j] = unique.Count;
                    unique.Add(single_data);
                }
            }
            return result;
        }
    }
}