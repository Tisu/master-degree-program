﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDataReader
{
    public class FileDataReader
    {
        private static readonly char[] SPLIT_CHARACTERS = { ',', ' ', '\t' };

        public List<List<string>> GetDataFromFile(string file_path)
        {
            var result = new List<List<string>>();
            var lines = File.ReadAllLines(file_path);
            foreach (var line in lines)
            {
                foreach (var split_character in SPLIT_CHARACTERS)
                {
                    var datas = line.Split(split_character);
                    if (datas.Length <= 1)
                    {
                        continue;
                    }

                    result.Add(datas.ToList());
                }
            }
            return result;
        }
    }
}
